* Getting started
  * [Your Virtual Machine](intro.md)
  * [The Command Line](command_line.md)
* [Simple Git](git.md)
* [Hello World](hello_world.md)
  * [Hello Again](hello_again_world.md)
* Variables
  * Introduction
  * Data Types
  * Assignments
  * Naming Variables
  * Immutable Object
  * Introduction
  * Object References
  * Copying Object References
* Exercises
* Python Structures
  * Introduction
  * Sequence Structure
  * The _if_ Section Construct
  * The _if... else_ Selection Construct
  * Boundary testing
  * Nested Selection Constructs
  * The _elif_ Selection Constructs
  * The _while_ (repetition loop)
* Exercises
* Python Operators
  * Introduction
  * Relational Operators
  * Multiple Conditional Tests
  * range function
  * The _for_ loop
  * Arithmetic operators
  * The _%_ operator
  * logical _and_ operator
  * Operator precedence
  * Multiple Assignment
  * Multiple Assignment and Trace Tables
  * Simultaneous Assignment
* Exercises
* Python Functions
  * Introduction
  * _ord()_ function
  * _chr()_ function
  * _eval()_ function
  * _print_ function
  * _abs_ function
  * _type_ function
  * _id()_ function
* Exercises
* Classes and Objects
  * Introduction
  * Class and Object
  * Binding names to object
  * String class and object
  * Empty string
  * Bind to an existing string
  * Augmented Operators and Binding to Objects
* Exercises
* Python Strings
  * Introduction
  * length
  * iterating
  * forward indexing
  * backward indexing
  * counting words
  * string replication
  * string slicing
  * extracting URIs
  * _find()_ string method
  * _index()_ method
  * _endswith()_ method
* Exercises
* User defined function
  * Introduction
  * how to design
  * function with arguments and no return value
  * function with no arguments but return value
  * function with no arguments and no return value
  * keyword arguments
  * default arguments
  * Local and Global variable scope
* Exercises
* Classes and Objects (optional/time permitting)
  * Introduction
  * Class Members
  * Construction and Assignment
  * Python Method Parameters and self
  * Instance Method
* Exercises
