# Introduction to programming with Python

July 5/6

Instructor: [Francis Kayiwa](https://twitter.com/kayiwa)

## Overview

This will be a full day workshop to introducing Python. The workshop will focus on concepts of python rather than idiomatic python. The target audience for the workshops is neophytes who are completely new to programming. The structure of the workshop is to remain self-driven with some guidance. This nature of this repo will be a "live" document that will be improved on by those who find bits of it missing. Pull requests are therefore most welcome.

### Software Installation etc.,

It is possible to use your standard Operating System python installation. The instructor is happy to discuss this outside of the workshop. However to allow for consistency we will be using a virtual machine to allow for consistency. It is my experience that the less hoops one has to jump through when learning the easier it is to comprehend the scope and applicability of concepts. 

### Code of Conduct

Attendees at this workshop are expected to abide by the [Code4lib Code of Conduct](http://2016.code4lib.org/conduct.html).

In addition I will be applying the Hacker school [social rules](https://www.recurse.com/manual#sub-sec-social-rules) for the entirety of the workshop to foster a healthy learning environment.

### Workshop materials

The overall [agenda is here](AGENDA.md)
