## Hello World

Log into your [pythonanywhere.com](https://pythonanywhere.com) account 

Click on python 3.4 to start a new console

![pythonanywhere dash](images/python_anywhere_dash.png)

type the following and hit the carriage return at the end of the line. 

This will launch a new Python3.4 console

![pythonanywhere 34consile](images/python_anywhere_console.png)

```
>>> message = 'Hello Melville!'
>>> print (message)
Hello Melville!
>>> message
'Hello Melville!'
>>> number = 42
>>> print (number)
42
>>> number
42
```


**What just happened?**

When you clicked on the 3.4 you launched the python anywhere interpreter which is viewed as a console. For your additional exercises you can read up on what the python interpreter is. For the rest of the workshop think of it as your assistant that converts english words into machine language.

```
>>> message = 'Hello Melville!' 
```

In this program statement we assign the string ’Hello world!’ to the variable “message”. 

```
>>> print (message)
```

takes the content of the variable ‘message’ and “prints” the output to the stdout (in this case the screen)

The next two lines

```
>>> number = 42
>>> print (number)
```

do effectively the same thing as the First two. Except this time we will be assigning an integer 42 to the variable

To complete this type 

```
>>> exit()
```

to close the console.

Click on the Dashboard and familiarize yourself with the pythonanywhere environment. For this workshop we will be focusing on Python 3.4 


